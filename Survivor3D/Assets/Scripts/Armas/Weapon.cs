using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject arrowPrefab;
    public Transform arrowSpawn;
    public float power = 20f;

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        GameObject arrow = Instantiate(arrowPrefab, arrowSpawn.position, arrowSpawn.rotation);
        arrow.GetComponent<Rigidbody>().AddForce(transform.forward * power, ForceMode.Impulse);
    }


}
