using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{

    public GameObject panelInfo;
    public GameObject panelColec;

    private void Start()
    {

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void Info()
    {
       panelInfo.SetActive(true);
    }

    public void Coleccionables()
    {
        panelColec.SetActive(true);
    }
    public void Cerrar()
    {
        panelColec.SetActive(false);
        panelInfo.SetActive(false);
    }
}
