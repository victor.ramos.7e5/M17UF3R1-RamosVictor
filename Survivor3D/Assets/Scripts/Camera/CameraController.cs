using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;


public class CameraController : MonoBehaviour
{
  
    
    public CinemachineFreeLook StandardCamSettings;
    public Transform CombatTarget;
    public Transform PlayerTarget;
    public float StandardFov;
    public float CombatFov;
    private float _fovChange;
    
    void Start()
    {
       
      
        _fovChange = StandardFov;
       
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            StandardCamSettings.LookAt = CombatTarget;
            _fovChange = Mathf.Clamp(_fovChange - 1, CombatFov, StandardFov);
            StandardCamSettings.m_Lens.FieldOfView = _fovChange;
        }
        else
        {
            StandardCamSettings.LookAt = PlayerTarget;
            _fovChange = Mathf.Clamp(_fovChange + 1, CombatFov, StandardFov);
            StandardCamSettings.m_Lens.FieldOfView = _fovChange;
        }


    }
}
