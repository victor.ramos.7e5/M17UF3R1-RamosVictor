using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class AimCamera : MonoBehaviour
{
    public Transform Camera;
    public float Sensitivity;

    // Start is called before the first frame update
    void Start()
    {
        if (Sensitivity == 0)
        {
            Sensitivity = .5f;
        }
    }

    // Update is called once per frame
    void Update()
    {
        var direction = (transform.position - new Vector3(Camera.transform.position.x, transform.position.y, Camera.transform.position.z)).normalized;
        transform.forward = direction;
        
    }
    /* public Cinemachine.AxisState xAxis, yAxis;
     [SerializeField] Transform camFollowPos;
     void Start()
     {

     }

     // Update is called once per frame
     void Update()
     {
         xAxis.Update(Time.deltaTime);
         yAxis.Update(Time.deltaTime);
     }

     private void LateUpdate()
     {
         camFollowPos.localEulerAngles = new Vector3(yAxis.Value, camFollowPos.localEulerAngles.y, camFollowPos.localEulerAngles.z);
         transform.eulerAngles = new Vector3(transform.eulerAngles.x, xAxis.Value, transform.eulerAngles.z);
     }*/
}
