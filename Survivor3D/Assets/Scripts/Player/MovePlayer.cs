using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    public float speed;
    private float originalspeed;


    private float hInput;
    private float vInput;
    public CharacterController player;
    [HideInInspector] public Vector3 dir;


    Vector3 moveVelocity;
    Vector3 turnVelocity;
    public float jumpforce;
    public float gravity;

    private Animator anim;
    private bool jump;

    private float alturaOriginal;
    private bool agachado = false;
    // La altura a la que se agachar� el personaje
    public float alturaAgachado = 1f;
    private bool apuntando = false;

    private float tiempocelebracion = 3;
    //private float tiempointeraccion = 3.5f;
    public GameObject camcele;
    void Start()
    {
        originalspeed = speed;
        alturaOriginal = player.height;
        anim = GetComponent<Animator>();
        player = GetComponent<CharacterController>();
       
    }

    void Update()
    {
        /* hInput = Input.GetAxis("Horizontal");
         vInput = Input.GetAxis("Vertical");*/

        anim.SetFloat("Velx", hInput);
        anim.SetFloat("Vely", vInput);

        Agacharse();
        Jump();
        Apuntar();

        Celebracion();
        InteractObject();

        // Gravedad
            moveVelocity.y += gravity * Time.deltaTime;
            player.Move(moveVelocity * Time.deltaTime);
            transform.Rotate(turnVelocity * Time.deltaTime);
       


       
    }
    private void FixedUpdate()
    {
        //if (!interaccion)
        //{
            GetDirectionMove();
        //}
        // player.Move(new Vector3(hInput, 0, vInput) * speed * Time.deltaTime);

    }

    void GetDirectionMove()
    {
        hInput = Input.GetAxis("Horizontal");
        vInput = Input.GetAxis("Vertical");

        dir = transform.forward * vInput + transform.right * hInput;

        player.Move(dir * speed * Time.deltaTime);
    }
    public void Celebracion()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {

            anim.SetTrigger("Celebracion");
            camcele.SetActive(true);
            StartCoroutine(EsperarAnimacion(tiempocelebracion));
        }
   

    }
   
    public void InteractObject()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {

            anim.SetTrigger("Pick");
            //StartCoroutine(EsperarAnimacion(tiempointeraccion));


        }

    }
    IEnumerator EsperarAnimacion(float tiempoespera)
    {
        var velocidadActual = speed;
        speed = 0;
        yield return new WaitForSeconds(tiempoespera);
        speed = velocidadActual;
        camcele.SetActive(false);


    }
    public void Jump()
    {
        if (player.isGrounded)
        {

            if (Input.GetButtonDown("Jump"))
            {
                jump = true;

                moveVelocity.y = jumpforce;

            }
        }
        else
        {
            jump = false;
        }
        anim.SetBool("IsJumping", jump);

    }


    public void Agacharse()
    {
        float reducirvelocidad = 2;
        if (Input.GetKeyDown(KeyCode.LeftControl) || Input.GetKeyDown(KeyCode.C))
        {
            if (!agachado)
            {
                agachado = true;
                anim.SetBool("Agachado", agachado);
                speed = speed / reducirvelocidad;

                /*player.height = alturaAgachado;
                player.center = new Vector3(0, alturaAgachado / 2f, 0);*/
            }
            else
            {
                agachado = false;
                anim.SetBool("Agachado", agachado);
                speed = originalspeed;
                /*player.height = alturaOriginal;
                player.center = new Vector3(0, alturaOriginal / 2f, 0);*/
            }
        }
    }
   
    public void Apuntar()
    {
        if (Input.GetKey(KeyCode.Mouse1))
        {
            apuntando = true;
            anim.SetBool("ApuntarArco", apuntando);
        }
        else
        {
            apuntando = false;

            anim.SetBool("ApuntarArco", apuntando);

        }
    }
}
