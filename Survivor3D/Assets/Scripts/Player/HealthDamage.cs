using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthDamage : MonoBehaviour, IDamageable
{
    public float life = 100;
    private bool invencible = false;
    public float tiempoInvencible;
    public float tiempoEspera;
    public float tiempoEsperaMuerte= 5;
    [SerializeField] private AudioClip audioSource;

    private Animator anim;


    public void Damage (float damageamount)
    {
        RestarVida(damageamount);
    }
    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    public void RestarVida(float damagerecive)
    {
        if (!invencible && life >0)
        {
            life -= damagerecive;
            AudioManager.Instance.EjecutarSonido(audioSource);

            anim.Play("ReactionDamage");
            StartCoroutine(Invulnerabilidad());
            StartCoroutine(EsperarAnimacion(tiempoEspera));
        }
        else if (life <=0)
        {
            anim.Play("Die");
            StartCoroutine(EsperarAnimacion(tiempoEsperaMuerte));
        }
    }

    IEnumerator Invulnerabilidad()
    {
        invencible = true;
        yield return new WaitForSeconds(tiempoInvencible);
        invencible = false;
    }


    IEnumerator EsperarAnimacion(float tiempo)
    {
        var velocidadActual = GetComponent<MovePlayer>().speed;
        GetComponent<MovePlayer>().speed = 0;
        yield return new WaitForSeconds(tiempo);
        //GetComponent<MovePlayer>().speed = velocidadActual;
        if (life>0)
        {
            GetComponent<MovePlayer>().speed = velocidadActual;
        }
        else
        {
            SceneManager.LoadScene(3);
        }
    }
}
