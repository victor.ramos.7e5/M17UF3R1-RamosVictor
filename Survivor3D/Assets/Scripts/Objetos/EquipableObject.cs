using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipableObject : Objects
{
    public GameObject equipObject;
    public Inventario inventario;
    [SerializeField] private AudioClip audioSource;
    public GameObject image;

    void Start()
    {
        inventario = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventario>();
       // equipObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            inventario.cantObjetos = inventario.cantObjetos + 1;
            equipObject.SetActive(true);
            image.SetActive(true);
            AudioManager.Instance.EjecutarSonido(audioSource);
            Destroy(this.gameObject);
        }
    }
}
