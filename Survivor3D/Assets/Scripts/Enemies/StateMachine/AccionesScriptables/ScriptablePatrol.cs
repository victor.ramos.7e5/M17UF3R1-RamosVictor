using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ScriptablePatrol", menuName = "ScriptableObjects/ScriptableAction/ScriptablePatrol", order = 3)]

public class ScriptablePatrol : ScriptableAction
{
    private MoveBehaviour _movePlayer;


    public override void OnFinishState()
    {
        _movePlayer.PatrolWaypoints();
    }

    public override void OnSetState(StateController sc)
    {

        _movePlayer = sc.GetComponent<MoveBehaviour>();

    }

    public override void OnUpdate(StateController sc)
    {       
        _movePlayer.PatrolWaypoints();


    }


}
