using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="ScriptableAttack",menuName = "ScriptableObjects/ScriptableAction/ScriptableAttack")]
public class ScriptableAttack : ScriptableAction
{
    private AttackBehaviour _attackBehaviour;
    private MoveBehaviour _moveBehaviour;
    private EnemyController _enemyController;

    public override void OnFinishState()
    {
        _moveBehaviour.Perseguir();

        //_attackBehaviour.AttackMelee();

    }

    public override void OnSetState(StateController sc)
    {

        _attackBehaviour = sc.GetComponent<AttackBehaviour>();
        _moveBehaviour = sc.GetComponent<MoveBehaviour>();
        _moveBehaviour.StopChase();
    }

    public override void OnUpdate(StateController sc)
    {
        _attackBehaviour.EnFrenteAlJugador();
        _attackBehaviour.Atacar();

    }


}
