using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController2 : StateController
{
    private float hp, damage, speed, range, cooldown;
    public EnemiesSO _SOenemy;
    private float rangodistanciar;
    private float rangodetenerse;
    float originalTime;
    private float timetoShoot = 1.3f;
    public NavMeshAgent agent;

    public ScriptableState idle;
    private Transform player;
    public ScriptableState Follow, Attack, Die,Trap;



    private bool isPlayerDetected = false;

    private void Start()
    {
        currentState.action.OnSetState(this);
        originalTime = timetoShoot;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        hp = _SOenemy.hp;
        range = _SOenemy.range;
        damage = _SOenemy.damage;
        speed = _SOenemy.speed;
        rangodistanciar = _SOenemy.rangodistanciar;
        rangodetenerse = _SOenemy.rangodetenerse;
        agent = GetComponent<NavMeshAgent>();
    }


    void Update()
    {
        currentState.action.OnUpdate(this);
        DetectPlayer();

    }
    private void DetectPlayer()
    {
        if (Vector3.Distance(transform.position, player.position) < range)
        {
            StateTransition(Follow);
            if (Vector3.Distance(transform.position, player.position) < range)
            {
                StateTransition(Attack);

            }
        }
    }


   
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag =="Player")
        {
            Death();
        }
        if (other.tag=="Trampa") // Poner tag de la tramp
        {
            Traped();
        }
    }
    private void Death()
    {
        StateTransition(Die);
    }
    private void Traped()
    {
        StateTransition(Trap);
        StartCoroutine(WaitTrapped());
    }
    IEnumerator WaitTrapped()
    {

        yield return new WaitForSeconds(5f);
        StateTransition(Follow);


    }
}
