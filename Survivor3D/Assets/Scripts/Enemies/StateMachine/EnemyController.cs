using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : StateController
{
    private float hp, damage, range;
    public EnemiesSO _SOenemy;
   
    //float originalTime;
    private float timetoShoot = 1.3f;
    public NavMeshAgent agent;

    public ScriptableState Patrol, Follow, Attack, Die;
    private Transform player;
    private Municion arrow;

    private Animator anim;

    private bool isPlayerDetected = false;

    private void Start()
    {
        currentState.action.OnSetState(this);
        //originalTime = timetoShoot;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        hp = _SOenemy.hp;
        range = _SOenemy.range;
        damage = _SOenemy.damage;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

    }


    void Update()
    {
       // Debug.Log(currentState);

        currentState.action.OnUpdate(this);
        DetectPlayer();
    }
    private void DetectPlayer()
    {
        if (!isPlayerDetected)
        {
            // Si el jugador no ha sido detectado, continuar patrullando entre los waypoints
            StateTransition(Patrol);
            if (Vector3.Distance(transform.position, player.position) < range)
            {
                isPlayerDetected = true;
            }
        }
        else
        {
            if (Vector3.Distance(transform.position, player.position) < 2)
            {
                StateTransition(Attack);

            }
            else if (Vector3.Distance(transform.position, player.position) > range)
            {
                isPlayerDetected = false;
            }
            else
            {
                StateTransition(Follow);
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Arrow"))
        {
           // Debug.Log("hit");
            TakeDamage(Municion.Instance.damage);

        }
    }
    

    public void TakeDamage(float damage)
    {
        hp -= damage;
        Debug.Log("Health: " + hp);
        if (hp<=0)
        {
            Death();
        }
    }


    
    private void Death()
    {
        StateTransition(Die);
    }
    
}
